# Nim Game

This is a very simple implementation of the famous Nim Game in the Misère variant:

1.  Put 13 matches on the table
2.  Each player pulls 1 to 3 matches, until... 
3.  ... one player has to pull the last match. That player looses the game.

This implemenation is a very rough playground of various technologies like REST APIs and Spring Boot.

# Features

*   REST API
*   based Spring Boot - self contained JAR

# Limitations
*  No UI. Swagger UI is used, but this should not count as UI, shouldn't it?
*  No persistence beside in-memory.
*  No concurrency control. Behavior is unpredicted, if human and CPU pulls in same time.

# Development
## Eclipse
1.  Clone this repository
2.  Use "Import Projects" wizard, select "Existing Gradle Project" and point the dialogue to the root directory of this project.

## Build
Build distributable by exeuting

```bash
./gradlew bootJar
```

## Run
Execute

```bash
java -jar ./build/libs/de.jbiblio.nimgame-0.1.0-SNAPSHOT.jar
```

and point your browser to http://localhost:8080/docs. There you'll find Swagger-UI presenting the OpenAPI documentation.

From there you're able to "play" by testing the endpoints of the REST API:
1.  call `POST /game`
2.  copy the `gameId` from the returned JSON
3.  call `POST /game/{gameId}/gameplay`
4.  call `POST /game/{gameId}/gameplay/nudgeCPU`
5.  repeat from step 3 until game is over, which redirects you to `/game/{gameId}/status`.

Sorry, no real UI exists.

Of course, you may build your own UI or use `curl` to play the game.