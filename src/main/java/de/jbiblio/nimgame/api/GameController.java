package de.jbiblio.nimgame.api;

import static org.springframework.web.util.UriComponentsBuilder.fromPath;

import java.util.List;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.jbiblio.nimgame.exception.GameNotFoundException;
import de.jbiblio.nimgame.model.Game;
import de.jbiblio.nimgame.service.GameService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping(path = "/game")
public class GameController {

	private final GameService gameService;

	public GameController(GameService gameService) {
		super();
		this.gameService = gameService;
	}

	@Operation(summary = "Create new game",
			description = "Creates a new game that is ready to be played and redirects to that game so the first matches can be pulled.")
	@ApiResponse(responseCode = "302 Found", description = "Redirect to /game/id so you're able to receive the game")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> createGame() {
		Game game = gameService.createGame();
		return ResponseEntity.status(HttpStatus.FOUND).location(fromPath("/game/{gameId}").build(game.getGameId()))
				.build();
	}

	@Operation(summary = "Deletes a game", description = "Deletes a given game and redirects to the list of all games.")
	@ApiResponse(responseCode = "200 OK",
			description = "OK to signal successful deletion and a Location-header pointing to a list of all games.")
	@DeleteMapping(path = "/{gameId}")
	public ResponseEntity<Void> deleteGame(@PathVariable UUID gameId) throws GameNotFoundException {
		gameService.deleteGame(gameId);
		return ResponseEntity.status(HttpStatus.OK).location(fromPath("/game").build().toUri()).build();
	}

	@Operation(summary = "List all games", description = "Lists all available games")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Game> getAllGames() {
		return gameService.getAllGames();
	}

	@Operation(summary = "Returns a game", description = "Returns a game by its ID")
	@ApiResponse(responseCode = "200", description = "If game was found")
	@GetMapping(path = "/{gameId}")
	public Game getGame(@Parameter(description = "ID of a game") @PathVariable UUID gameId)
			throws GameNotFoundException {
		return gameService.getGame(gameId).orElseThrow(() -> new GameNotFoundException(gameId));
	}

}
