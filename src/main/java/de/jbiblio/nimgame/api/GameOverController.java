package de.jbiblio.nimgame.api;

import java.util.Optional;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import de.jbiblio.nimgame.model.Game;
import de.jbiblio.nimgame.model.GameStatusReport;
import de.jbiblio.nimgame.model.Player;
import de.jbiblio.nimgame.service.GameService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
public class GameOverController {

	private GameService gameService;

	public GameOverController(GameService gameService) {
		super();
		this.gameService = gameService;
	}

	@Operation(summary = "Award ceremony",
			description = "Returns the award for the current player, or, if she lost, some hugs.")
	@ApiResponse(responseCode = "200 OK", description = "Game status")
	@GetMapping(path = "/game/{gameId}/status")
	public GameStatusReport getWinner(@PathVariable UUID gameId) {

		Game game = gameService.getGame(gameId).orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("%s Not Found", gameId)));

		Optional<Player> winner = game.getWinner();
		if (!winner.isPresent()) {
			return new GameStatusReport("No winner, yet");
		} else if (winner.get() == Player.CPU) {
			return new GameStatusReport("Human player lost");
		} else if (winner.get() == Player.HUMAN) {
			return new GameStatusReport("Human player won");
		} else {
			throw new IllegalStateException("Inconsistent State in Game " + gameId);
		}
	}
}