package de.jbiblio.nimgame.api;

import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import de.jbiblio.nimgame.exception.GameNotFoundException;
import de.jbiblio.nimgame.exception.IllegalMatchesException;
import de.jbiblio.nimgame.exception.WrongPlayerPulledException;
import de.jbiblio.nimgame.model.Game;
import de.jbiblio.nimgame.model.Player;
import de.jbiblio.nimgame.service.GameService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping(path = "/game/{gameId}/gameplay")
public class GamePlayController {

	private final GameService gameService;

	public GamePlayController(GameService gameService) {
		super();
		this.gameService = gameService;
	}

	@Operation(summary = "Nudge the CPU",
			description = "If it's CPUs player's turn, the human player may nudge the CPU to pull some matches. Will return updated game board.")
	@ApiResponses(value = { @ApiResponse(responseCode = "200 OK", description = "Updated game"),
			@ApiResponse(responseCode = "302 Found", description = "If game is over", content = @Content()) })
	@PostMapping(path = "nudgeCpu")
	public ResponseEntity<Void> nudgeCpu(@PathVariable UUID gameId)
			throws GameNotFoundException, IllegalMatchesException, WrongPlayerPulledException {
		Game game = gameService.nudgeCpuPlayer(gameId);
		if (!game.isGameOver()) {
			return ResponseEntity.status(HttpStatus.FOUND)
					.location(UriComponentsBuilder.fromPath("/game/{gameId}").build(gameId)).build();
		} else {
			return ResponseEntity.status(HttpStatus.FOUND)
					.location(UriComponentsBuilder.fromPath("/game/{gameId}/status").build(gameId)).build();
		}
	}

	@Operation(summary = "Pulls some matches",
			description = "If it's human player's turn, she may pull 1-3 matches. Will return the updated game board.")
	@ApiResponses(value = { @ApiResponse(responseCode = "200 OK", description = "Updated game"),
			@ApiResponse(responseCode = "302 Found", description = "If game is over", content = @Content()) })
	@PostMapping
	public ResponseEntity<Void> pullMatches(@PathVariable UUID gameId, @RequestParam int matchesToPull)
			throws GameNotFoundException, IllegalMatchesException, WrongPlayerPulledException {
		Game game = gameService.pullMatches(gameId, Player.HUMAN, matchesToPull);
		if (!game.isGameOver()) {
			return ResponseEntity.status(HttpStatus.FOUND)
					.location(UriComponentsBuilder.fromPath("/game/{gameId}").build(gameId)).build();
		} else {
			return ResponseEntity.status(HttpStatus.FOUND)
					.location(UriComponentsBuilder.fromPath("/game/{gameId}/status").build(gameId)).build();
		}
	}

}
