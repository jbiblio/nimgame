package de.jbiblio.nimgame.api;

import static org.springframework.web.util.UriComponentsBuilder.fromPath;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
public class RootController {

	@Operation(summary = "Main entry for Nim Game")
	@ApiResponses(value = { @ApiResponse(responseCode = "302 Found",
			description = "Redirect to /game for a list of existing games", content = @Content()) })
	@GetMapping(path = "/")
	@ResponseStatus(code = HttpStatus.FOUND)
	public ResponseEntity<Void> get() {
		return ResponseEntity.status(HttpStatus.FOUND).location(fromPath("/game").build().toUri()).build();
	}
}
