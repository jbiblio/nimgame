package de.jbiblio.nimgame.service;

import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Service;

import de.jbiblio.nimgame.model.Game;

/**
 * A CPU player implementation based on random selection of matches.
 */
@Service
class CpuPlayerRandomService {

	/**
	 * Returns the number of matches the CPU wants to pull.
	 *
	 * If only 1 match is left, CPU has lost and this method will return 0, so you
	 * should check game status before calling.
	 *
	 * @param matchesLeft The number of matches available on board
	 * @return Returns the number of matches the CPU wants to pull.
	 */
	public int getNumberOfMatchesToPull(int matchesLeft) {
		if (matchesLeft == 1) {
			// short-circuit, I've lost already
			return 0;
		} else if (matchesLeft == 2) {
			// short-circuit, 2 would mean I'm loosing :-o
			return 1;
		} else {
			return dice(matchesLeft);
		}
	}

	/**
	 * Dices the number of matches to pull. WoWill be between 1 and
	 * {@link Game#MAX_MATCHES_ALLOWED_TO_PULL}, inclusively.
	 *
	 * But won't pull the last match, since in this case CPU will loose the game.
	 *
	 * @param matchesLeft Number of matches left on board
	 * @return number of matches CPU likes to pull
	 */
	int dice(int matchesLeft) {
		// maxMatches -1, so at least one match will persist on board.
		int upperBound = Math.min(matchesLeft - 1, Game.MAX_MATCHES_ALLOWED_TO_PULL);
		// upper bound is exclusive, so +1
		return ThreadLocalRandom.current().nextInt(upperBound) + 1;
	}

}
