package de.jbiblio.nimgame.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Repository;

import de.jbiblio.nimgame.exception.GameNotFoundException;
import de.jbiblio.nimgame.model.Game;

/**
 * Simple in-memory implementation of a Game Repository, backed by a
 * {@link HashMap}.
 */
@Repository
class InMemoryGameRepository {

	private final Map<UUID, Game> games = new HashMap<>();

	/**
	 * Deletes given game.
	 *
	 * @param gameId {@link Game#getGameId() ID} of game to delete
	 * @throws GameNotFoundException if game with given ID does not exist
	 */
	public void delete(UUID gameId) throws GameNotFoundException {
		Game oldGame = games.remove(gameId);
		if (oldGame == null) {
			throw new GameNotFoundException(gameId);
		}
	}

	/**
	 * @return all existing games.
	 */
	public List<Game> getAllGames() {
		return new ArrayList<>(games.values());
	}

	/**
	 * @param gameId {@link Game#getGameId() ID} of game to return
	 * @return Game with given ID, if it exists.
	 */
	public Optional<Game> getGame(UUID gameId) {
		return Optional.ofNullable(games.get(gameId));
	}

	/**
	 * Saves given game.
	 *
	 * @param game Game to save
	 */
	public void persist(Game game) {
		games.put(game.getGameId(), game);
	}
}
