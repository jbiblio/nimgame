package de.jbiblio.nimgame.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import de.jbiblio.nimgame.exception.GameNotFoundException;
import de.jbiblio.nimgame.exception.IllegalMatchesException;
import de.jbiblio.nimgame.exception.WrongPlayerPulledException;
import de.jbiblio.nimgame.model.Game;
import de.jbiblio.nimgame.model.Player;

/**
 * Service definition for the Nim Game
 */
public interface GameService {

	/**
	 * Creates a new game.
	 *
	 * @return created game.
	 * @see #getGame(UUID)
	 */
	Game createGame();

	/**
	 * Deletes given game.
	 *
	 * @param gameId {@link Game#getGameId() ID} of game to delete.
	 * @throws GameNotFoundException If that game does not exist.
	 */
	void deleteGame(UUID gameId) throws GameNotFoundException;

	/**
	 * @return all existing games.
	 */
	List<Game> getAllGames();

	/**
	 * @param gameId {@link Game#getGameId() ID} of game to return
	 * @return Game with given ID
	 */
	Optional<Game> getGame(UUID gameId);

	/**
	 * Reminds the CPU to pull some matches.
	 *
	 * @param gameId {@link Game#getGameId() ID} of game to pull matches from
	 * @return updated game
	 * @throws GameNotFoundException      If game was not found
	 * @throws WrongPlayerPulledException If CPU is not on turn
	 */
	Game nudgeCpuPlayer(UUID gameId) throws GameNotFoundException, WrongPlayerPulledException;

	/**
	 * Pulls some matches from game.
	 *
	 * @param gameId        {@link Game#getGameId() ID} of game to pull matches from
	 * @param matchesToPull Number of matches to pull. See
	 *                      {@link Game#MAX_MATCHES_ALLOWED_TO_PULL}
	 * @return Updated game
	 * @throws GameNotFoundException      If game does not exist
	 * @throws IllegalMatchesException    If player pulls less than 1 or more than
	 *                                    {@value Game#MAX_MATCHES_ALLOWED_TO_PULL}
	 *                                    matches from game
	 * @throws WrongPlayerPulledException If this is not the player's turn
	 */
	Game pullMatches(UUID gameId, Player player, int matchesToPull)
			throws GameNotFoundException, IllegalMatchesException, WrongPlayerPulledException;

}
