package de.jbiblio.nimgame.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import de.jbiblio.nimgame.exception.GameNotFoundException;
import de.jbiblio.nimgame.exception.IllegalMatchesException;
import de.jbiblio.nimgame.exception.WrongPlayerPulledException;
import de.jbiblio.nimgame.model.Game;
import de.jbiblio.nimgame.model.Player;

@Service
class GameServiceImpl implements GameService {

	private CpuPlayerRandomService cpuPlayer;

	private InMemoryGameRepository repo;

	public GameServiceImpl(InMemoryGameRepository repo, CpuPlayerRandomService cpuPlayer) {
		super();
		this.repo = repo;
		this.cpuPlayer = cpuPlayer;
	}

	@Override
	public Game createGame() {
		Game game = new Game(UUID.randomUUID(), Player.HUMAN);
		repo.persist(game);
		return game;
	}

	@Override
	public void deleteGame(UUID gameId) throws GameNotFoundException {
		repo.delete(gameId);
	}

	@Override
	public List<Game> getAllGames() {
		return repo.getAllGames();
	}

	@Override
	public Optional<Game> getGame(UUID gameId) {
		return repo.getGame(gameId);
	}

	@Override
	public Game nudgeCpuPlayer(UUID gameId) throws GameNotFoundException, WrongPlayerPulledException {
		Game game = repo.getGame(gameId).orElseThrow(() -> new GameNotFoundException(gameId));

		int matchesLeft = game.getMatchesLeft();
		int matchesToPull = cpuPlayer.getNumberOfMatchesToPull(matchesLeft);
		try {
			return pullMatches(gameId, Player.CPU, matchesToPull);
		} catch (IllegalMatchesException exc) {
			throw new IllegalStateException("CPU pulls unsupported number of matches");
		}
	}

	@Override
	public Game pullMatches(UUID gameId, Player player, int matchesToPull)
			throws GameNotFoundException, IllegalMatchesException, WrongPlayerPulledException {

		Game game = repo.getGame(gameId).orElseThrow(() -> new GameNotFoundException(gameId));

		game.pullMatches(matchesToPull, player);
		// not needed in this simple in-memory implementation, but is'a a nice idea for
		// a database based implementation
		repo.persist(game);

		return game;
	}

}
