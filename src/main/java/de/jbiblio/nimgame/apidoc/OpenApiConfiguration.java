package de.jbiblio.nimgame.apidoc;

import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

/**
 * This configuration enables the OpenAPI documentation of the REST API of this
 * game. See {@code application.properties} for path.
 */
@OpenAPIDefinition(info = @Info(title = "Nim Game", description = "A simple implementation of the Nim Game"))
@Configuration
public class OpenApiConfiguration {

}
