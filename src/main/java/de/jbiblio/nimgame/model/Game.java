package de.jbiblio.nimgame.model;

import java.util.Optional;
import java.util.UUID;

import de.jbiblio.nimgame.exception.IllegalMatchesException;
import de.jbiblio.nimgame.exception.WrongPlayerPulledException;

/**
 * A Nim Game instance.
 */
public class Game {

	public static final int INITIAL_MATCHES = 13;

	/**
	 * The maximum number of matches a player is allowed to pull. If less then 3
	 * matches are left, a player should adopt and pull less then 3 matches.
	 */
	public static final int MAX_MATCHES_ALLOWED_TO_PULL = 3;

	private Player currentPlayer;

	private final UUID gameId;

	private int matchesLeft;

	private Optional<Player> winner = Optional.empty();

	/**
	 * Creates a new instance.
	 *
	 * @param gameId      Unique ID of this instance.
	 * @param startPlayer The player that will begin the game by
	 *                    {@link #pullMatches() pulling} the first match.
	 */
	public Game(UUID gameId, Player startPlayer) {
		this(gameId, startPlayer, Game.INITIAL_MATCHES);
	}

	/**
	 * Creates a new instance.
	 *
	 * @param gameId         Unique ID of this instance.
	 * @param startPlayer    The player that will begin the game by
	 *                       {@link #pullMatches() pulling} the first match.
	 * @param initialMatches Number of initial matches
	 */
	public Game(UUID gameId, Player startPlayer, int initialMatches) {
		super();
		this.gameId = gameId;
		this.currentPlayer = startPlayer;
		this.matchesLeft = initialMatches;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	/**
	 * @return game's ID
	 */
	public UUID getGameId() {
		return gameId;
	}

	/**
	 *
	 * @return number of matches left on board
	 */
	public int getMatchesLeft() {
		return matchesLeft;
	}

	/**
	 *
	 * @return the winner of this game, if the game is won already.
	 * @see #isGameOver()
	 */
	public Optional<Player> getWinner() {
		return winner;
	}

	/**
	 *
	 * @return <code>true</code> if game is over and {@link #getWinner()} contains
	 *         the winner.
	 */
	public boolean isGameOver() {
		return getWinner().isPresent();
	}

	/**
	 * Pulls a single match from board.
	 *
	 * @param matches       Number of matches to pull. Must be 1..3.
	 * @param pullingPlayer The player that pulls the matches
	 * @throws IllegalMatchesException    if less than 1 or more than 3 matches were
	 *                                    given.
	 * @throws IllegalMatchesException    if board is empty, see
	 *                                    {@link #getMatchesLeft()}
	 * @throws WrongPlayerPulledException if the {@code pullingPlayer} is not the
	 *                                    {@link #getCurrentPlayer() current player}
	 */
	public void pullMatches(int matches, Player pullingPlayer)
			throws IllegalMatchesException, WrongPlayerPulledException {
		if (matchesLeft == 1) {
			throw new IllegalMatchesException(gameId, matches);
		} else if (pullingPlayer != currentPlayer) {
			throw new WrongPlayerPulledException(gameId, currentPlayer);
		} else if (matchesLeft == 0) {
			throw new IllegalMatchesException(gameId, matches);
		} else if (matches < 1 || matches > Game.MAX_MATCHES_ALLOWED_TO_PULL) {
			throw new IllegalMatchesException(gameId, matchesLeft, matches);
		}
		matchesLeft -= matches;

		if (matchesLeft == 1) {
			winner = Optional.of(currentPlayer);
		} else {
			currentPlayer = pullingPlayer.toggle();
		}
	}
}
