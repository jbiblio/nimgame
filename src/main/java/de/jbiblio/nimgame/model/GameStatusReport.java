package de.jbiblio.nimgame.model;

/**
 * Simple report about game's status.
 */
public class GameStatusReport {

	private final String status;

	public GameStatusReport(String status) {
		super();
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
}
