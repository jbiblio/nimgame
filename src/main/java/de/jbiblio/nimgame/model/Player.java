package de.jbiblio.nimgame.model;

/**
 * The type of a player.
 */
public enum Player {
	/**
	 * Player is CPU-driven.
	 */
	CPU,

	/**
	 * A brain-driven user.
	 */
	HUMAN;

	public Player toggle() {
		switch (this) {
		case CPU:
			return HUMAN;
		case HUMAN:
			return CPU;
		default:
			throw new IllegalArgumentException("Cannot toggle from player player");
		}
	}
}
