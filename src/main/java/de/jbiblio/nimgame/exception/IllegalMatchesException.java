package de.jbiblio.nimgame.exception;

import java.util.UUID;

import de.jbiblio.nimgame.model.Game;

/**
 * This Exception will be thrown if player pulls illegal number of matches (less
 * than 1, more than {@value Game#MAX_MATCHES_ALLOWED_TO_PULL}).
 */
public class IllegalMatchesException extends Exception {
	private static final long serialVersionUID = 1L;

	private final UUID gameId;

	private int matchesLeft;

	private final int numberOfMatchesPulled;

	public IllegalMatchesException(UUID gameId, int numberOfMatchesPulled) {
		super("You must pull 1.." + Game.MAX_MATCHES_ALLOWED_TO_PULL + " maches, but not " + numberOfMatchesPulled
				+ " from game with id " + gameId);
		this.gameId = gameId;
		this.numberOfMatchesPulled = numberOfMatchesPulled;
	}

	public IllegalMatchesException(UUID gameId, int matchesLeft, int numberOfMatchesPulled) {
		super("You cannot pull " + numberOfMatchesPulled + " matches from game with id " + gameId + ", only "
				+ matchesLeft + " matchesLeft");
		this.gameId = gameId;
		this.matchesLeft = matchesLeft;
		this.numberOfMatchesPulled = numberOfMatchesPulled;
	}

	public UUID getGameId() {
		return gameId;
	}

	public int getMatchesLeft() {
		return matchesLeft;
	}

	public int getNumberOfMatchesPulled() {
		return numberOfMatchesPulled;
	}
}
