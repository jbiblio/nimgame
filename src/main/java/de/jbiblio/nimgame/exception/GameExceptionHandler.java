package de.jbiblio.nimgame.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GameExceptionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(GameExceptionHandler.class);

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<Void> handle(GameNotFoundException exc) {
		LOG.debug(exc.getMessage(), exc);
		return ResponseEntity.notFound().build();
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<Void> handle(IllegalMatchesException exc) {
		LOG.debug(exc.getMessage(), exc);
		return ResponseEntity.badRequest().build();
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<Void> handle(WrongPlayerPulledException exc) {
		LOG.debug(exc.getMessage(), exc);
		return ResponseEntity.badRequest().build();
	}
}
