package de.jbiblio.nimgame.exception;

import java.util.UUID;

import de.jbiblio.nimgame.model.Player;

/**
 * This Exception will be thrown if the wrong player tries to pull some matches.
 */
public class WrongPlayerPulledException extends Exception {
	private static final long serialVersionUID = 1L;

	private Player currentPlayer;

	private final UUID gameId;

	public WrongPlayerPulledException(UUID gameId, Player currentPlayer) {
		super("Wrong player tried to pull on game with id " + gameId + ". It's " + currentPlayer + "'s turn");
		this.gameId = gameId;
		this.currentPlayer = currentPlayer;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public UUID getGameId() {
		return gameId;
	}
}
