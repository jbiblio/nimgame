package de.jbiblio.nimgame.exception;

import java.util.UUID;

/**
 * This Exception is thrown if game with given ID does not exist.
 */
public class GameNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	private final UUID gameId;

	public GameNotFoundException(UUID gameId) {
		super("Game with id " + gameId + " not found");
		this.gameId = gameId;
	}

	public UUID getGameId() {
		return gameId;
	}
}
