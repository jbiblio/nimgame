package de.jbiblio.nimgame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NimGame {

	public static void main(String[] args) {
		SpringApplication.run(NimGame.class, args);
	}

}
