package de.jbiblio.nimgame.model;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

public class PlayerTest {

	@Test
	public void testToggle() {
		assertThat(Player.CPU.toggle(), is(Player.HUMAN));
		assertThat(Player.HUMAN.toggle(), is(Player.CPU));
	}

}
