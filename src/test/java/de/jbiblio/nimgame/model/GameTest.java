package de.jbiblio.nimgame.model;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.UUID;

import org.junit.jupiter.api.Test;

import de.jbiblio.nimgame.exception.IllegalMatchesException;
import de.jbiblio.nimgame.exception.WrongPlayerPulledException;

public class GameTest {

	@Test
	public void testBoardHasDefaultMatches() {
		Game game = new Game(UUID.randomUUID(), Player.HUMAN);
		assertThat(game.getMatchesLeft(), is(13));
	}

	@Test
	public void testGameOver() throws IllegalMatchesException, WrongPlayerPulledException {
		// GIVEN
		Game game = new Game(UUID.randomUUID(), Player.HUMAN, 2);

		// THEN
		assertThat(game.isGameOver(), is(false));

		// WHEN
		game.pullMatches(1, Player.HUMAN);

		// THEN
		assertThat(game.isGameOver(), is(true));

	}

	@Test
	public void testIntegrityOfMatches() {
		// GIVEN
		Player player = Player.HUMAN;
		Game game = new Game(UUID.randomUUID(), player);

		// THEN
		assertThrows(IllegalMatchesException.class, () -> game.pullMatches(-1, player));
		assertThrows(IllegalMatchesException.class, () -> game.pullMatches(0, player));
		assertThrows(IllegalMatchesException.class, () -> game.pullMatches(4, player));

	}

	@Test
	public void testPullAllMatchesPlusOne() throws IllegalMatchesException, WrongPlayerPulledException {
		// GIVEN empty board
		Player player = Player.HUMAN;
		Game game = new Game(UUID.randomUUID(), player, 1);

		assertThrows(IllegalMatchesException.class, () -> game.pullMatches(1, player));
	}

	@Test
	public void testPullMatches() throws IllegalMatchesException, WrongPlayerPulledException {
		// GIVEN
		Player player = Player.HUMAN;
		Game game = new Game(UUID.randomUUID(), player);

		// WHEN
		game.pullMatches(2, player);

		// THEN
		assertThat(game.getMatchesLeft(), is(11));
		assertThat(game.getCurrentPlayer(), is(Player.CPU));

		// WHEN
		game.pullMatches(2, Player.CPU);
		assertThat(game.getCurrentPlayer(), is(Player.HUMAN));
	}

	@Test
	public void testPullMatchesAsWrongPlayer() {
		// GIVEN
		Game game = new Game(UUID.randomUUID(), Player.HUMAN);

		// THEN
		assertThrows(WrongPlayerPulledException.class, () -> game.pullMatches(2, Player.CPU));
	}

}
