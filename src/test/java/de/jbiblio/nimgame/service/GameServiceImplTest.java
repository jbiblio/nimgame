package de.jbiblio.nimgame.service;

import static java.util.Optional.empty;
import static java.util.UUID.randomUUID;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.lessThan;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import de.jbiblio.nimgame.exception.GameNotFoundException;
import de.jbiblio.nimgame.exception.IllegalMatchesException;
import de.jbiblio.nimgame.exception.WrongPlayerPulledException;
import de.jbiblio.nimgame.model.Game;
import de.jbiblio.nimgame.model.Player;

public class GameServiceImplTest {

	private GameServiceImpl sut;

	@BeforeEach
	public void setup() {
		InMemoryGameRepository repo = new InMemoryGameRepository();
		CpuPlayerRandomService cpuPlayer = new CpuPlayerRandomService();

		sut = new GameServiceImpl(repo, cpuPlayer);
	}

	@Test
	public void testCreateGame() {
		// WHEN
		Game game = sut.createGame();

		// THEN
		assertThat(game, is(notNullValue()));
	}

	@Test
	public void testDeleteGame() {
		// GIVEN
		Game game = sut.createGame();

		// WHEN
		try {
			sut.deleteGame(game.getGameId());
		} catch (GameNotFoundException e) {
			fail("Should not occur when deleting existing games", e);
		}

		// THEN
		assertThat(sut.getGame(game.getGameId()), is(empty()));
	}

	@Test
	public void testDeleteNonExistentGame() {
		assertThrows(GameNotFoundException.class, () -> sut.deleteGame(UUID.randomUUID()));
	}

	@Test
	public void testGetAllGames() {
		// GIVEN
		for (int i = 0; i < 5; ++i) {
			sut.createGame();
		}

		// WHEN
		List<Game> allGames = sut.getAllGames();

		// THEN
		assertThat(allGames.size(), is(5));
	}

	@Test
	public void testGetGame() {
		// GIVEN
		Game newGame = sut.createGame();

		// WHEN
		Optional<Game> game = sut.getGame(newGame.getGameId());

		// THEN
		assertThat(game.get().getGameId(), is(newGame.getGameId()));
		assertThat(game.get().getCurrentPlayer(), is(Player.HUMAN));

	}

	@Test
	public void testGetNonExistentGame() {
		// WHEN
		Optional<Game> game = sut.getGame(randomUUID());

		// THEN
		assertThat(game, is(empty()));
	}

	@RepeatedTest(value = 12)
	public void testNudgeCpu() throws GameNotFoundException, IllegalMatchesException, WrongPlayerPulledException {
		// GIVEN
		Game game = sut.createGame();
		sut.pullMatches(game.getGameId(), Player.HUMAN, 3);
		int matchesLeft = sut.getGame(game.getGameId()).get().getMatchesLeft();

		// WHEN
		sut.nudgeCpuPlayer(game.getGameId());

		// THEN
		assertThat(sut.getGame(game.getGameId()).get().getMatchesLeft(), is(lessThan(matchesLeft)));

	}

	@Test
	public void testPullFromNonExstentGame() throws GameNotFoundException, IllegalMatchesException {
		assertThrows(GameNotFoundException.class, () -> sut.pullMatches(UUID.randomUUID(), Player.HUMAN, 1));
	}

	@Test
	public void testPullMatches() throws GameNotFoundException, IllegalMatchesException, WrongPlayerPulledException {
		// GIVEN
		Game game = sut.createGame();

		// WHEN
		sut.pullMatches(game.getGameId(), Player.HUMAN, 2);

		// THEN
		assertThat(sut.getGame(game.getGameId()).get().getMatchesLeft(), is(11));
	}
}
