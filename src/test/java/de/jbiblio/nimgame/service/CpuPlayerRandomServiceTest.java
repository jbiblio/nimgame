package de.jbiblio.nimgame.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.lessThanOrEqualTo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import de.jbiblio.nimgame.model.Game;

public class CpuPlayerRandomServiceTest {

	private CpuPlayerRandomService cpu;

	@BeforeEach
	public void given() {
		cpu = new CpuPlayerRandomService();
	}

	@Test
	public void testCpuPullsDicedValueOnStart() {
		// WHEN
		int numberOfMatchesToPull = cpu.getNumberOfMatchesToPull(Game.INITIAL_MATCHES);

		// THEN
		assertThat(numberOfMatchesToPull, lessThanOrEqualTo(Game.MAX_MATCHES_ALLOWED_TO_PULL));
	}

	@RepeatedTest(value = 5)
	public void testCpuPullsLessThan3MatchesAtTheEnd() {
		// WHEN
		int numberOfMatchesToPull = cpu.getNumberOfMatchesToPull(3);

		// THEN
		assertThat(numberOfMatchesToPull, lessThanOrEqualTo(2));
	}

	@Test
	public void testCpuPullsLessTheLastPossibleMatchWithoutLoosing() {
		// WHEN
		int numberOfMatchesToPull = cpu.getNumberOfMatchesToPull(2);

		// THEN
		assertThat(numberOfMatchesToPull, is(1));
	}
}
