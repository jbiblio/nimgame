package de.jbiblio.nimgame;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@SpringBootTest
@AutoConfigureMockMvc
class RootControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void testRootRedirectsToListOfGames() throws Exception {
		// WHEN
		ResultActions res = mockMvc.perform(get("/"));

		// THEN
		res.andExpect(status().isFound());
		res.andExpect(redirectedUrl("/game"));
	}

}
