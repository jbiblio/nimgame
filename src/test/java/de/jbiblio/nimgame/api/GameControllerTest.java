package de.jbiblio.nimgame.api;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlTemplate;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import de.jbiblio.nimgame.exception.GameNotFoundException;
import de.jbiblio.nimgame.model.Game;
import de.jbiblio.nimgame.model.Player;
import de.jbiblio.nimgame.service.GameService;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {

	@MockBean
	private GameService gs;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void testCreateGame() throws Exception {
		// GIVEN
		UUID gameId = UUID.randomUUID();
		when(gs.createGame()).thenReturn(new Game(gameId, Player.HUMAN));

		// WHEN
		ResultActions res = mockMvc.perform(post("/game"));

		// THEN
		res.andExpect(status().isFound());
		res.andExpect(redirectedUrlTemplate("/game/{gameId}", gameId));
	}

	@Test
	void testDeleteGame() throws Exception {
		UUID gameToDelete = UUID.randomUUID();

		// WHEN
		ResultActions res = mockMvc.perform(delete("/game/{gameId}", gameToDelete));

		// THEN
		res.andExpect(status().isOk());
	}

	@Test
	void testDeleteNonExistentGame() throws Exception {
		// GIVEN
		doThrow(GameNotFoundException.class).when(gs).deleteGame(Mockito.any(UUID.class));

		// WHEN
		ResultActions res = mockMvc.perform(delete("/game/{gameId}", UUID.randomUUID()));

		// THEN
		res.andExpect(status().isNotFound());
	}

	@Test
	void testGetAllGames() throws Exception {
		// WHEN
		ResultActions res = mockMvc.perform(get("/game"));

		// THEN
		res.andExpect(status().isOk());
	}

	@Test
	void testGetGame() throws Exception {
		// GIVEN
		UUID gameToFetch = UUID.randomUUID();
		Player player = Player.CPU;

		when(gs.getGame(gameToFetch)).thenReturn(Optional.of(new Game(gameToFetch, player)));

		// WHEN
		ResultActions res = mockMvc.perform(get("/game/{gameId}", gameToFetch));

		// THEN
		res.andExpect(status().isOk());
		res.andExpect(jsonPath("gameId", is(gameToFetch.toString())));
		res.andExpect(jsonPath("matchesLeft", is(13)));
		res.andExpect(jsonPath("currentPlayer", is(player.name())));
	}

	@Test
	void testGetNonExistentGame() throws Exception {
		// GIVEN
		when(gs.getGame(Mockito.any(UUID.class))).thenReturn(Optional.empty());

		// WHEN
		ResultActions res = mockMvc.perform(get("/game/{gameId}", UUID.randomUUID()));

		// THEN
		res.andExpect(status().isNotFound());
	}

}
