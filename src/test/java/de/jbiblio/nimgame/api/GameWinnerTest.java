package de.jbiblio.nimgame.api;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.UUID;

import org.junit.jupiter.api.Test;

import de.jbiblio.nimgame.exception.IllegalMatchesException;
import de.jbiblio.nimgame.exception.WrongPlayerPulledException;
import de.jbiblio.nimgame.model.Game;
import de.jbiblio.nimgame.model.Player;

public class GameWinnerTest {

	@Test
	public void testCpuLooses() throws IllegalMatchesException, WrongPlayerPulledException {
		// GIVEN
		Game game = new Game(UUID.randomUUID(), Player.HUMAN, 3);

		// WHEN
		game.pullMatches(2, Player.HUMAN);

		// THEN
		assertThat(game.getWinner().get(), is(Player.HUMAN));
	}

	@Test
	public void testCpuWins() throws IllegalMatchesException, WrongPlayerPulledException {
		// GIVEN
		Game game = new Game(UUID.randomUUID(), Player.HUMAN, 3);

		// WHEN
		game.pullMatches(1, Player.HUMAN);
		game.pullMatches(1, Player.CPU);

		// THEN
		assertThat(game.getWinner().get(), is(Player.CPU));
	}

	@Test
	public void testRunningGame() {
		Game game = new Game(UUID.randomUUID(), Player.HUMAN);

		assertThat(game.getWinner().isPresent(), is(false));
	}

}
