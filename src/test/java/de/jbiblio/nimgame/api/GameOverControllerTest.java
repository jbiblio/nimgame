package de.jbiblio.nimgame.api;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import de.jbiblio.nimgame.model.Game;
import de.jbiblio.nimgame.model.Player;
import de.jbiblio.nimgame.service.GameService;

@SpringBootTest
@AutoConfigureMockMvc
class GameOverControllerTest {

	@MockBean
	private GameService gs;

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testCpuWon() throws Exception {
		// GIVEN
		when(gs.getGame(any(UUID.class))).thenReturn(Optional.of(new Game(UUID.randomUUID(), Player.CPU) {
			@Override
			public Optional<Player> getWinner() {
				return Optional.of(Player.CPU);
			}
		}));

		// WHEN
		ResultActions res = mockMvc.perform(get("/game/{gameId}/status", UUID.randomUUID()));

		// THEN
		res.andExpect(status().isOk());
		res.andExpect(jsonPath("status", is("Human player lost")));
	}

	@Test
	public void testGameNotOverYert() throws Exception {
		// GIVEN
		when(gs.getGame(any(UUID.class))).thenReturn(Optional.of(new Game(UUID.randomUUID(), Player.CPU) {
			@Override
			public Optional<Player> getWinner() {
				return Optional.empty();
			}
		}));

		// WHEN
		ResultActions res = mockMvc.perform(get("/game/{gameId}/status", UUID.randomUUID()));

		// THEN
		res.andExpect(status().isOk());
		res.andExpect(jsonPath("status", is("No winner, yet")));
	}

	@Test
	public void testHumanWon() throws Exception {
		// GIVEN
		when(gs.getGame(any(UUID.class))).thenReturn(Optional.of(new Game(UUID.randomUUID(), Player.CPU) {
			@Override
			public Optional<Player> getWinner() {
				return Optional.of(Player.HUMAN);
			}
		}));

		// WHEN
		ResultActions res = mockMvc.perform(get("/game/{gameId}/status", UUID.randomUUID()));

		// THEN
		res.andExpect(status().isOk());
		res.andExpect(jsonPath("status", is("Human player won")));
	}

}
