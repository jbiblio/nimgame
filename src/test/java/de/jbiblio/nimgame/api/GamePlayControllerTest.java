package de.jbiblio.nimgame.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlTemplate;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import de.jbiblio.nimgame.exception.GameNotFoundException;
import de.jbiblio.nimgame.exception.IllegalMatchesException;
import de.jbiblio.nimgame.model.Game;
import de.jbiblio.nimgame.model.Player;
import de.jbiblio.nimgame.service.GameService;

@SpringBootTest
@AutoConfigureMockMvc
class GamePlayControllerTest {

	@MockBean
	private GameService gs;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void testCpuPullsLastMatch() throws Exception {
		// GIVEN
		// only one match is left
		UUID gameId = UUID.randomUUID();
		when(gs.nudgeCpuPlayer(any())).thenReturn(new Game(gameId, Player.CPU, 1) {

			@Override
			public Optional<Player> getWinner() {
				return Optional.of(Player.CPU);
			}
		});

		// WHEN
		ResultActions res = mockMvc.perform(post("/game/{gameId}/gameplay/nudgeCpu", gameId));

		// THEN
		res.andExpect(status().isFound());
		res.andExpect(redirectedUrlTemplate("/game/{gameId}/status", gameId));
	}

	@Test
	void testHumanPullsLastMatch() throws Exception {
		// GIVEN
		// only one match is left
		UUID gameId = UUID.randomUUID();
		when(gs.pullMatches(any(), any(), anyInt())).thenReturn(new Game(gameId, Player.HUMAN, 1) {

			@Override
			public Optional<Player> getWinner() {
				return Optional.of(Player.HUMAN);
			}
		});

		// WHEN
		ResultActions res = mockMvc.perform(post("/game/{gameId}/gameplay?matchesToPull={matchesToPull}", gameId, 1));

		// THEN
		res.andExpect(status().isFound());
		res.andExpect(redirectedUrlTemplate("/game/{gameId}/status", gameId));
	}

	@Test
	void testNudge() throws Exception {
		// GIVEN
		// only one match is left
		UUID gameId = UUID.randomUUID();
		when(gs.nudgeCpuPlayer(any())).thenReturn(new Game(gameId, Player.HUMAN, 8));

		// WHEN
		ResultActions res = mockMvc.perform(post("/game/{gameId}/gameplay/nudgeCpu", gameId));

		// THEN
		res.andExpect(status().isFound());
		res.andExpect(redirectedUrlTemplate("/game/{gameId}", gameId));
	}

	@Test
	void testPullMatches() throws Exception {
		// GIVEN
		// only one match is left
		UUID gameId = UUID.randomUUID();
		when(gs.pullMatches(any(), any(), anyInt())).thenReturn(new Game(gameId, Player.HUMAN));

		// WHEN
		ResultActions res = mockMvc.perform(post("/game/{gameId}/gameplay?matchesToPull={matchesToPull}", gameId, 2));

		// THEN
		res.andExpect(status().isFound());
		res.andExpect(redirectedUrlTemplate("/game/{gameId}", gameId));

	}

	@Test
	void testPullOnNonExistendGame() throws Exception {
		// GIVEN
		doThrow(GameNotFoundException.class).when(gs).pullMatches(any(UUID.class), any(Player.class), anyInt());

		// WHEN
		ResultActions res = mockMvc
				.perform(post("/game/{gameId}/gameplay?matchesToPull={matchesToPull}", UUID.randomUUID(), 0));

		// THEN
		res.andExpect(status().isNotFound());
	}

	@Test
	void testPullWrongNumberOfMatches() throws Exception {
		// GIVEN
		doThrow(IllegalMatchesException.class).when(gs).pullMatches(any(UUID.class), any(Player.class), anyInt());

		// WHEN
		ResultActions res = mockMvc
				.perform(post("/game/{gameId}/gameplay?matchesToPull={matchesToPull}", UUID.randomUUID(), 0));

		// THEN
		res.andExpect(status().isBadRequest());
	}

}
